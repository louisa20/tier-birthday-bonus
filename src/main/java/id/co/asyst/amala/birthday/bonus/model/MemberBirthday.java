package id.co.asyst.amala.birthday.bonus.model;

public class MemberBirthday {
    private String email;
    private String tierid;
    private String channel;
    private String branchcode;
    private String partnercode;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTierid() {
        return tierid;
    }

    public void setTierid(String tierid) {
        this.tierid = tierid;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getBranchcode() {
        return branchcode;
    }

    public void setBranchcode(String branchcode) {
        this.branchcode = branchcode;
    }

    public String getPartnercode() {
        return partnercode;
    }

    public void setPartnercode(String partnercode) {
        this.partnercode = partnercode;
    }
}
