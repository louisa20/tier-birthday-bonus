package id.co.asyst.amala.birthday.bonus.utils;

import id.co.asyst.amala.birthday.bonus.model.MemberBirthday;
import id.co.asyst.amala.birthday.bonus.model.TierBirthdayBonus;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Jul 09, 2021
 * @since 1.2
 */

public class Consumer {
    private static final Logger logger = LogManager.getLogger();

    public void setTierbirthdaybonuslist(Exchange exchange) {
        List<Map<String, Object>> tierBirthdayBonuses = exchange.getProperty("tierbonusidlist", List.class);
        exchange.setProperty("tierbirthdaybonusgroup", tierbonusGroup(tierBirthdayBonusSet(tierBirthdayBonuses)));
    }

    /*used*/
    /*validate to get tierbonusid*/
    public void setMemberbirthdayAndTierbonus(Exchange exchange) {
        Message in = exchange.getIn();
        Map<String, Object> memberbirthday = in.getBody(Map.class);
        Map<String, Object> tierbirthdaybonusgroup = exchange.getProperty("tierbirthdaybonusgroup", Map.class);
        MemberBirthday memberBirthday = memberBirthdaySet(memberbirthday);
        exchange.setProperty("email", memberbirthday.get("email"));
        exchange.setProperty("tierbonusid", validateToGetTierbonusid(memberBirthday, tierbirthdaybonusgroup, memberGroup(memberBirthday)));
//        exchange.setProperty("body", memberBirthday);
    }

    /*used*/
    /*member group*/
    private List<String> memberGroup(MemberBirthday memberBirthday) {
        List<String> membergroup = new ArrayList<>();
        if ((memberBirthday.getTierid() != null) && (memberBirthday.getChannel() != null) && (memberBirthday.getBranchcode() != null) && (memberBirthday.getPartnercode() != null)) {
            membergroup.add("1");
            membergroup.add("2");
            membergroup.add("3");
            membergroup.add("4");
            membergroup.add("5");
            membergroup.add("6");
            membergroup.add("7");
            membergroup.add("8");
        } else if ((memberBirthday.getTierid() != null) && (memberBirthday.getChannel() != null) && (memberBirthday.getBranchcode() == null) && (memberBirthday.getPartnercode() != null)) {
            membergroup.add("2");
            membergroup.add("4");
            membergroup.add("6");
            membergroup.add("8");
        } else if ((memberBirthday.getTierid() != null) && (memberBirthday.getChannel() != null) && (memberBirthday.getBranchcode() != null) && (memberBirthday.getPartnercode() == null)) {
            membergroup.add("3");
            membergroup.add("4");
            membergroup.add("7");
            membergroup.add("8");
        } else if ((memberBirthday.getTierid() != null) && (memberBirthday.getChannel() != null) && (memberBirthday.getBranchcode() == null) && (memberBirthday.getPartnercode() == null)) {
            membergroup.add("4");
            membergroup.add("8");
        } else if ((memberBirthday.getTierid() != null) && (memberBirthday.getChannel() == null) && (memberBirthday.getBranchcode() != null) && (memberBirthday.getPartnercode() != null)) {
            membergroup.add("5");
            membergroup.add("6");
            membergroup.add("7");
            membergroup.add("8");
        } else if ((memberBirthday.getTierid() != null) && (memberBirthday.getChannel() == null) && (memberBirthday.getBranchcode() == null) && (memberBirthday.getPartnercode() != null)) {
            membergroup.add("6");
            membergroup.add("8");
        } else if ((memberBirthday.getTierid() != null) && (memberBirthday.getChannel() == null) && (memberBirthday.getBranchcode() != null) && (memberBirthday.getPartnercode() == null)) {
            membergroup.add("7");
            membergroup.add("8");
        } else if ((memberBirthday.getTierid() != null) && (memberBirthday.getChannel() == null) && (memberBirthday.getBranchcode() == null) && (memberBirthday.getPartnercode() == null))
            membergroup.add("8");
        return membergroup;
    }

    /*used*/
    /*validate v2*/
    private String validateToGetTierbonusid(MemberBirthday memberBirthday, Map<String, Object> tierbirthdaybonusgroup, List<String> membergroup) {
        String tierbonusid = "";
        for (String memberg : membergroup) {
            if (tierbonusid.length() == 0) {
                try {
                    List<TierBirthdayBonus> tierbirthdaybonusList = (List<TierBirthdayBonus>) tierbirthdaybonusgroup.get(memberg);
                    for (TierBirthdayBonus tierBirthdayBonus : tierbirthdaybonusList) {
                        if (tierbonusid.length() == 0) {
                            tierbonusid = matchingTierbirthdaybonus(memberBirthday, tierBirthdayBonus);
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
        return tierbonusid;
    }

    /*used*/
    /*match tierbirthdaybonus*/
    private String matchingTierbirthdaybonus(MemberBirthday memberBirthday, TierBirthdayBonus tierBirthdayBonus) {
        int next = 0;
        if (next == 0) {
            if (tierBirthdayBonus.getTierid() != null) {
                try {
                    if (memberBirthday.getTierid().equals(tierBirthdayBonus.getTierid()))
                        next = 0;
                    else
                        next = 1;
                } catch (Exception e) {
                    next = 1;
                }
            }
        }

        if (next == 0) {
            if (tierBirthdayBonus.getChannel() != null) {
                try {
                    if (memberBirthday.getChannel().equals(tierBirthdayBonus.getChannel()))
                        next = 0;
                    else
                        next = 1;
                } catch (Exception e) {
                    next = 1;
                }
            }
        }

        if (next == 0) {
            if (tierBirthdayBonus.getBranchcode() != null) {
                try {
                    if (memberBirthday.getBranchcode().equals(tierBirthdayBonus.getBranchcode()))
                        next = 0;
                    else
                        next = 1;
                } catch (Exception e) {
                    next = 1;
                }
            }
        }

        if (next == 0) {
            if (tierBirthdayBonus.getPartnercode() != null) {
                try {
                    if (memberBirthday.getPartnercode().equals(tierBirthdayBonus.getPartnercode()))
                        next = 0;
                    else
                        next = 1;
                } catch (Exception e) {
                    next = 1;
                }
            }
        }
        if (next == 0)
            return tierBirthdayBonus.getTierbonusid();
        return "";
    }

    /*used*/
    /* set memberbirthday */
    private MemberBirthday memberBirthdaySet(Map<String, Object> body) {
        MemberBirthday memberBirthday = new MemberBirthday();
        /* set memberid */
        try {
            memberBirthday.setEmail(body.get("email").toString());
        } catch (Exception e) {
            memberBirthday.setEmail(null);
        }
        /* set tierid */
        try {
            memberBirthday.setTierid(body.get("tierid").toString());
        } catch (Exception e) {
            memberBirthday.setTierid(null);
        }
        /* set channel */
        try {
            memberBirthday.setChannel(body.get("channel").toString());
        } catch (Exception e) {
            memberBirthday.setChannel(null);
        }
        /* set branchcode */
        try {
            memberBirthday.setBranchcode(body.get("branchcode").toString());
        } catch (Exception e) {
            memberBirthday.setBranchcode(null);
        }
        /* set partnercode */
        try {
            memberBirthday.setPartnercode(body.get("partnercode").toString());
        } catch (Exception e) {
            memberBirthday.setPartnercode(null);
        }
        return memberBirthday;
    }

    /*used*/
    /* set tierbirthdaybonus */
    private List<TierBirthdayBonus> tierBirthdayBonusSet(List<Map<String, Object>> body) {
        List<TierBirthdayBonus> tierBirthdayBonusList = new ArrayList<>();
        for (Map<String, Object> tbbl : body) {
            TierBirthdayBonus birthdayBonus = new TierBirthdayBonus();
            /* set tierbirthdaybonus */
            try {
                birthdayBonus.setTierbonusid(tbbl.get("tierbonusid").toString());
            } catch (Exception e) {
                birthdayBonus.setTierbonusid(null);
            }
            /* set tierid */
            try {
                birthdayBonus.setTierid(tbbl.get("tierid").toString());
            } catch (Exception e) {
                birthdayBonus.setTierid(null);
            }
            /* set channel */
            try {
                birthdayBonus.setChannel(tbbl.get("channel").toString());
            } catch (Exception e) {
                birthdayBonus.setChannel(null);
            }
            /* set branchcode */
            try {
                birthdayBonus.setBranchcode(tbbl.get("branchcode").toString());
            } catch (Exception e) {
                birthdayBonus.setBranchcode(null);
            }
            /* set partnercode */
            try {
                birthdayBonus.setPartnercode(tbbl.get("partnercode").toString());
            } catch (Exception e) {
                birthdayBonus.setPartnercode(null);
            }
            tierBirthdayBonusList.add(birthdayBonus);
        }
        return tierBirthdayBonusList;
    }

    /*used*/
    private Map<String, Object> tierbonusGroup(List<TierBirthdayBonus> tierBirthdayBonusList) {
        Map<String, Object> tierbonusgroup = new HashMap<>();
        List<TierBirthdayBonus> tierBirthdayBonusList1 = new ArrayList<>();
        List<TierBirthdayBonus> tierBirthdayBonusList2 = new ArrayList<>();
        List<TierBirthdayBonus> tierBirthdayBonusList3 = new ArrayList<>();
        List<TierBirthdayBonus> tierBirthdayBonusList4 = new ArrayList<>();
        List<TierBirthdayBonus> tierBirthdayBonusList5 = new ArrayList<>();
        List<TierBirthdayBonus> tierBirthdayBonusList6 = new ArrayList<>();
        List<TierBirthdayBonus> tierBirthdayBonusList7 = new ArrayList<>();
        List<TierBirthdayBonus> tierBirthdayBonusList8 = new ArrayList<>();
        for (TierBirthdayBonus tierbonus : tierBirthdayBonusList) {
            if ((tierbonus.getTierid() != null) && (tierbonus.getChannel() != null) && (tierbonus.getBranchcode() != null) && (tierbonus.getPartnercode() != null))
                tierBirthdayBonusList1.add(tierbonus);
            else if ((tierbonus.getTierid() != null) && (tierbonus.getChannel() != null) && (tierbonus.getBranchcode() == null) && (tierbonus.getPartnercode() != null))
                tierBirthdayBonusList2.add(tierbonus);
            else if ((tierbonus.getTierid() != null) && (tierbonus.getChannel() != null) && (tierbonus.getBranchcode() != null) && (tierbonus.getPartnercode() == null))
                tierBirthdayBonusList3.add(tierbonus);
            else if ((tierbonus.getTierid() != null) && (tierbonus.getChannel() != null) && (tierbonus.getBranchcode() == null) && (tierbonus.getPartnercode() == null))
                tierBirthdayBonusList4.add(tierbonus);
            else if ((tierbonus.getTierid() != null) && (tierbonus.getChannel() == null) && (tierbonus.getBranchcode() != null) && (tierbonus.getPartnercode() != null))
                tierBirthdayBonusList5.add(tierbonus);
            else if ((tierbonus.getTierid() != null) && (tierbonus.getChannel() == null) && (tierbonus.getBranchcode() == null) && (tierbonus.getPartnercode() != null))
                tierBirthdayBonusList6.add(tierbonus);
            else if ((tierbonus.getTierid() != null) && (tierbonus.getChannel() == null) && (tierbonus.getBranchcode() != null) && (tierbonus.getPartnercode() == null))
                tierBirthdayBonusList7.add(tierbonus);
            else if ((tierbonus.getTierid() != null) && (tierbonus.getChannel() == null) && (tierbonus.getBranchcode() == null) && (tierbonus.getPartnercode() == null))
                tierBirthdayBonusList8.add(tierbonus);
        }
        if (tierBirthdayBonusList1.size() > 0)
            tierbonusgroup.put("1", tierBirthdayBonusList1);
        if (tierBirthdayBonusList2.size() > 0)
            tierbonusgroup.put("2", tierBirthdayBonusList2);
        if (tierBirthdayBonusList3.size() > 0)
            tierbonusgroup.put("3", tierBirthdayBonusList3);
        if (tierBirthdayBonusList4.size() > 0)
            tierbonusgroup.put("4", tierBirthdayBonusList4);
        if (tierBirthdayBonusList5.size() > 0)
            tierbonusgroup.put("5", tierBirthdayBonusList5);
        if (tierBirthdayBonusList6.size() > 0)
            tierbonusgroup.put("6", tierBirthdayBonusList6);
        if (tierBirthdayBonusList7.size() > 0)
            tierbonusgroup.put("7", tierBirthdayBonusList7);
        if (tierBirthdayBonusList8.size() > 0)
            tierbonusgroup.put("8", tierBirthdayBonusList8);
        return tierbonusgroup;
    }

    /*used*/
    public void failedsendemail(Exchange exchange) {
        List<String> emailnotsend = exchange.getProperty("emailnotsend", List.class);
        emailnotsend.add(exchange.getProperty("email").toString());
        exchange.setProperty("emailnotsend", emailnotsend);
    }

//    public void setTieridbonus(Exchange exchange) {
//        Message in = exchange.getIn();
//        Map<String, Object> body = in.getBody(Map.class);
//        MemberBirthday memberBirthday = memberBirthdaySet(body);
//        List<TierBirthdayBonus> tierBirthdayBonus = tierBirthdayBonusSet(exchange.getProperty("tierbonusidlist", List.class));
//        String tierbonusid = "";
//        MemberBirthday memberBirthdayValidation = new MemberBirthday();
//        try {
//            String tierbonusidselected = "";
//            /* channel, branchcode, partnercode */
//            tierbonusidselected = validateToGetTierbonusid(true, false, false, false, memberBirthday, tierBirthdayBonus);
//            if (tierbonusidselected != null)
//                tierbonusid = tierbonusidselected;
//            if (memberBirthday.getChannel() != null) {
//                /* branchcode, partnercode */
//                tierbonusidselected = validateToGetTierbonusid(true, true, false, false, memberBirthday, tierBirthdayBonus);
//                if (tierbonusidselected != null)
//                    tierbonusid = tierbonusidselected;
//                if (memberBirthday.getBranchcode() != null) {
//                    /* partnercode */
//                    tierbonusidselected = validateToGetTierbonusid(true, true, true, false, memberBirthday, tierBirthdayBonus);
//                    if (tierbonusidselected != null)
//                        tierbonusid = tierbonusidselected;
//                    if (memberBirthday.getPartnercode() != null) {
//                        /*  */
//                        tierbonusidselected = validateToGetTierbonusid(true, true, true, true, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel != null, branchcode != null and partnercode != null");
//                        System.out.println(body);
//                    } else {
//                        /* partnercode */
//                        tierbonusidselected = validateToGetTierbonusid(true, true, true, false, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel != null, branchcode != null and partnercode == null");
//                        System.out.println(body);
//                    }
//                } else {
//                    /* branchcode, partnercode */
//                    tierbonusidselected = validateToGetTierbonusid(true, true, false, false, memberBirthday, tierBirthdayBonus);
//                    if (tierbonusidselected != null)
//                        tierbonusid = tierbonusidselected;
//                    if (memberBirthday.getPartnercode() != null) {
//                        /* branchcode */
//                        tierbonusidselected = validateToGetTierbonusid(true, true, false, true, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel != null, branchcode == null and partnercode != null");
//                        System.out.println(body);
//                    } else {
//                        /* branchcode, partnercode */
//                        tierbonusidselected = validateToGetTierbonusid(true, true, false, false, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel != null, branchcode == null and partnercode == null");
//                        System.out.println(body);
//                    }
//                }
//            } else {
//                /* channel, branchcode, partnercode */
//                tierbonusidselected = validateToGetTierbonusid(true, false, false, false, memberBirthday, tierBirthdayBonus);
//                if (tierbonusidselected != null)
//                    tierbonusid = tierbonusidselected;
//                if (memberBirthday.getBranchcode() != null) {
//                    /* channel, partnercode */
//                    tierbonusidselected = validateToGetTierbonusid(true, false, true, false, memberBirthday, tierBirthdayBonus);
//                    if (tierbonusidselected != null)
//                        tierbonusid = tierbonusidselected;
//                    if (memberBirthday.getPartnercode() != null) {
//                        /* channel */
//                        tierbonusidselected = validateToGetTierbonusid(true, false, true, true, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel == null, branchcode != null and partnercode != null");
//                        System.out.println(body);
//                    } else {
//                        /* channel, partnercode */
//                        tierbonusidselected = validateToGetTierbonusid(true, false, true, false, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel == null, branchcode != null and partnercode == null");
//                        System.out.println(body);
//                    }
//                } else {
//                    /* channel, branchcode, partnercode */
//                    tierbonusidselected = validateToGetTierbonusid(true, false, false, false, memberBirthday, tierBirthdayBonus);
//                    if (tierbonusidselected != null)
//                        tierbonusid = tierbonusidselected;
//                    if (memberBirthday.getPartnercode() != null) {
//                        /* channel, branchcode */
//                        tierbonusidselected = validateToGetTierbonusid(true, false, false, true, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel == null, branchcode == null and partnercode != null");
//                        System.out.println(body);
//                    } else {
//                        /* tanpa channel, branchcode, partnercode */
//                        tierbonusidselected = validateToGetTierbonusid(true, false, false, false, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel == null, branchcode == null and partnercode == null");
//                        System.out.println(body);
//                    }
//                }
//            }
//        } catch (Exception e) {
//            logger.info(body.get("memberid") + " error not matching with all tierbonus");
//        }
//        logger.info("tierbonusid "+tierbonusid);
//        exchange.setProperty("tierbonusid", tierbonusid);
//    }
//    /* validate tierbirthdaybonus */
//    private String validateToGetTierbonusid(Boolean tier, Boolean channel, Boolean branchcode, Boolean partnercode ,MemberBirthday memberBirthday, List<TierBirthdayBonus> tierBirthdayBonusList) {
//        for (TierBirthdayBonus tierBirthdayBonus : tierBirthdayBonusList) {
//            int next = 0;
//            /* tierid */
//            if (next == 0) {
//                if (tier.equals(Boolean.TRUE)) {
//                    try {
//                        if (memberBirthday.getTierid().equals(tierBirthdayBonus.getTierid()))
//                            next = 0;
//                        else
//                            next = 1;
//                    } catch (Exception e) {
//                        next = 1;
//                    }
//                } else {
//                    if (tierBirthdayBonus.getTierid() == null)
//                        next = 0;
//                    else
//                        next = 1;
//                }
//            }
//
//            /* channel */
//            if (next == 0) {
//                if (channel.equals(Boolean.TRUE)) {
//                    try {
//                        if (memberBirthday.getChannel().equals(tierBirthdayBonus.getChannel()))
//                            next = 0;
//                        else
//                            next = 1;
//                    } catch (Exception e) {
//                        next = 1;
//                    }
//                } else {
//                    if (tierBirthdayBonus.getChannel() == null)
//                        next = 0;
//                    else
//                        next = 1;
//                }
//            }
//
//            /* branchcode */
//            if (next == 0) {
//                if (branchcode.equals(Boolean.TRUE)) {
//                    try {
//                        if (memberBirthday.getBranchcode().equals(tierBirthdayBonus.getBranchcode()))
//                            next = 0;
//                        else
//                            next = 1;
//                    } catch (Exception e) {
//                        next = 1;
//                    }
//                } else {
//                    if (tierBirthdayBonus.getBranchcode() == null)
//                        next = 0;
//                    else
//                        next = 1;
//                }
//            }
//
//            /* partnercode */
//            if (next == 0) {
//                if (partnercode.equals(Boolean.TRUE)) {
//                    try {
//                        if (memberBirthday.getPartnercode().equals(tierBirthdayBonus.getPartnercode()))
//                            next = 0;
//                        else
//                            next = 1;
//                    } catch (Exception e) {
//                        next = 1;
//                    }
//                } else {
//                    if (tierBirthdayBonus.getPartnercode() == null)
//                        next = 0;
//                    else
//                        next = 1;
//                }
//            }
//
//            if (next == 0)
//                return tierBirthdayBonus.getTierbonusid();
//        }
//        return null;
//    }
//    public void exampleValidation (Exchange exchange) {
////        Message in = exchange.getIn();
////        Map<String, Object> body = in.getBody(Map.class);
//        Map<String, Object> memberbirthday = exchange.getProperty("member", Map.class);
//        MemberBirthday memberBirthday = memberBirthdaySet(memberbirthday);
////        List<Map<String, Object>> tierbirthdaybonus = exchange.getProperty("tierbirthdaybonus", List.class);
//        List<TierBirthdayBonus> tierBirthdayBonusList = tierBirthdayBonusSet(exchange.getProperty("tier", List.class));
//        exchange.setProperty("tierbonusid",validation(memberBirthday, tierBirthdayBonusList));
//    }
//
//    private String validation(MemberBirthday memberBirthday, List<TierBirthdayBonus> tierBirthdayBonus) {
//        String tierbonusid = "";
//        try {
//            String tierbonusidselected = "";
//            /* channel, branchcode, partnercode */
//            tierbonusidselected = validateToGetTierbonusid(true, false, false, false, memberBirthday, tierBirthdayBonus);
//            if (tierbonusidselected != null)
//                tierbonusid = tierbonusidselected;
//            if (memberBirthday.getChannel() != null) {
//                /* branchcode, partnercode */
//                tierbonusidselected = validateToGetTierbonusid(true, true, false, false, memberBirthday, tierBirthdayBonus);
//                if (tierbonusidselected != null)
//                    tierbonusid = tierbonusidselected;
//                if (memberBirthday.getBranchcode() != null) {
//                    /* partnercode */
//                    tierbonusidselected = validateToGetTierbonusid(true, true, true, false, memberBirthday, tierBirthdayBonus);
//                    if (tierbonusidselected != null)
//                        tierbonusid = tierbonusidselected;
//                    if (memberBirthday.getPartnercode() != null) {
//                        /*  */
//                        tierbonusidselected = validateToGetTierbonusid(true, true, true, true, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel != null, branchcode != null and partnercode != null");
////                        System.out.println(body);
//                    } else {
//                        /* partnercode */
//                        tierbonusidselected = validateToGetTierbonusid(true, true, true, false, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel != null, branchcode != null and partnercode == null");
////                        System.out.println(body);
//                    }
//                } else {
//                    /* branchcode, partnercode */
//                    tierbonusidselected = validateToGetTierbonusid(true, true, false, false, memberBirthday, tierBirthdayBonus);
//                    if (tierbonusidselected != null)
//                        tierbonusid = tierbonusidselected;
//                    if (memberBirthday.getPartnercode() != null) {
//                        /* branchcode */
//                        tierbonusidselected = validateToGetTierbonusid(true, true, false, true, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel != null, branchcode == null and partnercode != null");
////                        System.out.println(body);
//                    } else {
//                        /* branchcode, partnercode */
//                        tierbonusidselected = validateToGetTierbonusid(true, true, false, false, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel != null, branchcode == null and partnercode == null");
////                        System.out.println(body);
//                    }
//                }
//            } else {
//                /* channel, branchcode, partnercode */
//                tierbonusidselected = validateToGetTierbonusid(true, false, false, false, memberBirthday, tierBirthdayBonus);
//                if (tierbonusidselected != null)
//                    tierbonusid = tierbonusidselected;
//                if (memberBirthday.getBranchcode() != null) {
//                    /* channel, partnercode */
//                    tierbonusidselected = validateToGetTierbonusid(true, false, true, false, memberBirthday, tierBirthdayBonus);
//                    if (tierbonusidselected != null)
//                        tierbonusid = tierbonusidselected;
//                    if (memberBirthday.getPartnercode() != null) {
//                        /* channel */
//                        tierbonusidselected = validateToGetTierbonusid(true, false, true, true, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel == null, branchcode != null and partnercode != null");
////                        System.out.println(body);
//                    } else {
//                        /* channel, partnercode */
//                        tierbonusidselected = validateToGetTierbonusid(true, false, true, false, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel == null, branchcode != null and partnercode == null");
////                        System.out.println(body);
//                    }
//                } else {
//                    /* channel, branchcode, partnercode */
//                    tierbonusidselected = validateToGetTierbonusid(true, false, false, false, memberBirthday, tierBirthdayBonus);
//                    if (tierbonusidselected != null)
//                        tierbonusid = tierbonusidselected;
//                    if (memberBirthday.getPartnercode() != null) {
//                        /* channel, branchcode */
//                        tierbonusidselected = validateToGetTierbonusid(true, false, false, true, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel == null, branchcode == null and partnercode != null");
////                        System.out.println(body);
//                    } else {
//                        /* tanpa channel, branchcode, partnercode */
//                        tierbonusidselected = validateToGetTierbonusid(true, false, false, false, memberBirthday, tierBirthdayBonus);
//                        if (tierbonusidselected != null)
//                            tierbonusid = tierbonusidselected;
//                        logger.info("member with tierid != null, channel == null, branchcode == null and partnercode == null");
////                        System.out.println(body);
//                    }
//                }
//            }
//        } catch (Exception e) {
//            logger.info(memberBirthday.getMemberid() + " error not matching with all tierbonus");
//        }
//        return tierbonusid;
////        try {
////            String tierbonusidselected = "";
////            /* channel, branchcode, partnercode */
////            if (memberBirthday.getChannel() != null) {
////                /* branchcode, partnercode */
////                if (memberBirthday.getBranchcode() != null) {
////                    /* partnercode */
////                    if (memberBirthday.getPartnercode() != null) {
////                        /*  */
////                        logger.info("member with tierid != null, channel != null, branchcode != null and partnercode != null");
////                    } else {
////                        /* partnercode */
////                        logger.info("member with tierid != null, channel != null, branchcode != null and partnercode == null");
////
////                    }
////                } else {
////                    /* branchcode, partnercode */
////                    if (memberBirthday.getPartnercode() != null) {
////                        /* branchcode */
////                        logger.info("member with tierid != null, channel != null, branchcode == null and partnercode != null");
////                    } else {
////                        /* branchcode, partnercode */
////                        logger.info("member with tierid != null, channel != null, branchcode == null and partnercode == null");
////                    }
////                }
////            } else {
////                /* channel, branchcode, partnercode */
////                if (memberBirthday.getBranchcode() != null) {
////                    /* channel, partnercode */
////                    if (memberBirthday.getPartnercode() != null) {
////                        /* channel */
////                        logger.info("member with tierid != null, channel == null, branchcode != null and partnercode != null");
////                    } else {
////                        /* channel, partnercode */
////                        logger.info("member with tierid != null, channel == null, branchcode != null and partnercode == null");
////                    }
////                } else {
////                    /* channel, branchcode, partnercode */
////                    if (memberBirthday.getPartnercode() != null) {
////                        /* channel, branchcode */
////                        logger.info("member with tierid != null, channel == null, branchcode == null and partnercode != null");
////                    } else {
////                        /* tanpa channel, branchcode, partnercode */
////                        logger.info("member with tierid != null, channel == null, branchcode == null and partnercode == null");
////                    }
////                }
////            }
////        } catch (Exception e) {
////            logger.info(memberBirthday.getMemberid() + " error not matching with all tierbonus");
////        }
//    }
}