package id.co.asyst.amala.birthday.bonus.utils;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Jul 09, 2021
 * @since 1.2
 */

public class SimpleAggregatorStrategy implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        return newExchange;
    }
}