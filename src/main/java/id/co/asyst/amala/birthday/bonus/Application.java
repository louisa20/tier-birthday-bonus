package id.co.asyst.amala.birthday.bonus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;


@SpringBootApplication
//@ImportResource({"tier-birthday-bonus-scheduler-producer.xml", "beans.xml"})
@ImportResource({"tier-birthday-bonus-scheduler-consumer.xml", "beans.xml"})
public class Application {

    public static void main(String[] args) throws Exception {

        SpringApplication.run(Application.class, args);
    }
}