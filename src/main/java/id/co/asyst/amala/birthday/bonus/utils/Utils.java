package id.co.asyst.amala.birthday.bonus.utils;

import com.google.gson.Gson;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.component.redis.RedisConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Jul 09, 2021
 * @since 1.2
 */

public class Utils {
    private static final Gson gson = new Gson();
    private static final Logger logger = LogManager.getLogger();

    public void generateDateTimeNow(Exchange exchange) {
        Date date = new Date();
        SimpleDateFormat formatSimpleDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatSimpleDateTimeYMD = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatSimpleDateTimeYMDslash = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatSimpleDate = new SimpleDateFormat("yyyyMMdd");
        exchange.setProperty("datetimenow", formatSimpleDateTime.format(date));
        exchange.setProperty("datenow", formatSimpleDate.format(date));
        exchange.setProperty("datenowYMD", formatSimpleDateTimeYMD.format(date));
        exchange.setProperty("datenowYMDslash", formatSimpleDateTimeYMDslash.format(date));
    }

    public void inventory(Exchange exchange) {
        List<String> bodykafka = new ArrayList<>();
        exchange.setProperty("bodykafka", bodykafka);
        List<String> emailnotsend = new ArrayList<>();
        exchange.setProperty("emailnotsend", emailnotsend);
    }

    public void setTierBithday(Exchange exchange) {
        Message in = exchange.getIn();
        List<Map<String, Object>> tierbirthdaybonusretrieveresult = (List<Map<String, Object>>) in.getBody();
        List<Map<String, Object>> tierbirthdaybonuslist = new ArrayList<>();
        String[] event = {"tierid", "channel", "branchcode", "partnercode", "tierbonusid"};
        List<String> tieridlist = new ArrayList<>();

        for (int h = 0; h < tierbirthdaybonusretrieveresult.size(); h++) {
            Map<String, Object> tbbrr = tierbirthdaybonusretrieveresult.get(h);
            Map<String, Object> tierbirthdaybonus = new HashMap<>();
            for (int i = 0; i < event.length; i++) {
                if (event[i].equals("tierid")) {
                    tieridlist.add(tbbrr.get(event[i]).toString());
                    tierbirthdaybonus.put(event[i], tbbrr.get(event[i]));
                } else if (event[i].equals("channel")) {
                    try {
                        Map<String, Object> channel = (Map<String, Object>) tbbrr.get("channel");
                        tierbirthdaybonus.put(event[i], channel.get("channelid"));
                    } catch (Exception e) {
                        tierbirthdaybonus.put(event[i], null);
                    }
                } else
                    tierbirthdaybonus.put(event[i], tbbrr.get(event[i]));
            }
            tierbirthdaybonuslist.add(tierbirthdaybonus);
        }

        exchange.setProperty("tierbirthdaybonuslist", tierbirthdaybonuslist);
        exchange.setProperty("tieridlist", tieridlist.stream().distinct().collect(Collectors.toList()));
        exchange.setProperty("tierbirthdaybonusgroup", tierbirthdaybonusgroup(tieridlist.stream().distinct().collect(Collectors.toList()), tierbirthdaybonuslist));
        logger.info("tierbirthdaybonuslist = " + tierbirthdaybonuslist);
        logger.info("tieridlist = " + tieridlist.stream().distinct().collect(Collectors.toList()));
        logger.info("tierbirthdaybonusgroup" + tierbirthdaybonusgroup(tieridlist.stream().distinct().collect(Collectors.toList()), tierbirthdaybonuslist));
    }

    public Map<String, Object> tierbirthdaybonusgroup(List<String> tieridlist, List<Map<String, Object>> tierbirthdaybonuslist) {
        Map<String, Object> tierbirthdaybonusgroup = new HashMap<>();
        for (int i = 0; i < tieridlist.size(); i++) {
            List<Map<String, Object>> tierbirthday = new ArrayList<>();
            for (Map<String, Object> tierbirthdaybonus : tierbirthdaybonuslist) {
                if (tierbirthdaybonus.get("tierid").equals(tieridlist.get(i))) {
                    tierbirthday.add(tierbirthdaybonus);
                }
            }
            tierbirthdaybonusgroup.put(tieridlist.get(i), tierbirthday);
        }
        return tierbirthdaybonusgroup;
    }

    public void setKeyRedis(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        List<String> bodykafka = exchange.getProperty("bodykafka", List.class);
        Map<String, Object> body = in.getBody(Map.class);
        Map<String, Object> tierbirthdaybonuslist = exchange.getProperty("tierbirthdaybonusgroup", Map.class);
        int next = 1;
        for (Map.Entry tierbirthdaybonus : tierbirthdaybonuslist.entrySet()) {
            if (tierbirthdaybonus.getKey().equals(body.get("tierid"))) {
                Map<String, Object> k = new HashMap<>();
                Map<String, Object> bodySendToKafka = new HashMap<>();
                for (Map.Entry map : body.entrySet()) {
                    if (map.getKey().equals("tierid") || map.getKey().equals("channel") || map.getKey().equals("branchcode") || map.getKey().equals("partnercode"))
                        k.put(map.getKey().toString(), map.getValue());
                }
                String key = gson.toJson(k);
                String value = gson.toJson(body);
                logger.info("key ---> " + key.hashCode());
                logger.info("value ---> " + value);
                bodySendToKafka.put("keyredis", String.valueOf(key.hashCode()));
                bodySendToKafka.put("tierbonusidlist", tierbirthdaybonuslist.get(body.get("tierid")));
                bodykafka.add(gson.toJson(bodySendToKafka));
                out.setHeader(RedisConstants.KEY, key.hashCode());
                out.setHeader(RedisConstants.VALUE, value);
                next = 0;
            }
        }
        exchange.setProperty("next", next);
        exchange.setProperty("bodykafka", bodykafka.stream().distinct().collect(Collectors.toList()));
    }
}