package id.co.asyst.amala.birthday.bonus.model;

public class TierBirthdayBonus {
    private String tierid;

    private String tierbonusid;

    private String channel;

    private String branchcode;

    private String partnercode;

    public String getTierid() {
        return tierid;
    }

    public void setTierid(String tierid) {
        this.tierid = tierid;
    }

    public String getBranchcode() {
        return branchcode;
    }

    public void setBranchcode(String branchcode) {
        this.branchcode = branchcode;
    }

    public String getPartnercode() {
        return partnercode;
    }

    public void setPartnercode(String partnercode) {
        this.partnercode = partnercode;
    }

    public String getTierbonusid() {
        return tierbonusid;
    }

    public void setTierbonusid(String tierbonusid) {
        this.tierbonusid = tierbonusid;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
